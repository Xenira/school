# Datensicherheit

| Technische Maßnahmen | Organisatorische Maßnahmen |
| --- | --- |
| Authentisierung, z.B.: Passwortvergabe | Beachtung von Regeln für Passwörter |
| Biometrische Authentisierungsverfahren |  |
| Datenverschlüsselung | - privaten Schlüssel sicher aufheben (z.B. Secure Smart Card nach HBCI-Norm 4)<br>- aktuelle Verschlüsselungsverfahren mit ausreichend langen Schlüsseln (z.B.4096 Bit bei asymm. Verfahren - RSA / 256 Bit bei symm. Verfahren)<br>- Zertifikate von vertrauenswürdigen Zertifizierungsstellen |