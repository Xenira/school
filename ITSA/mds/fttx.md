# Glasfaserbasierte Zugangsnetze (FTTx)

- Fiber, to the
- Glasfaseranbindung der "letzten Meile"
- Fiber to the
   1. H (Home) - Bis zum Hausverteiler
   1. B (Basement) - Bis zum Hausverteiler
   1. C (Curb) - Bis zum Gehstieg
   1. N (Node) - Bis zum Kabelverzweiger
   1. Ex (Exchange) - Bis zur Vermittlungsstelle
- Vorteile gegenueber Telefonleitungen (DSL)
   - hoehere Uebertragungsgeschwindigkeit
   - keine Elektromagnetische Felder
- Uebertragungsmodell: ATM (Asynchronus Transfer Mode)

## Aufgaben zu Kapitel "6.7.4 Oeffentliche kabelgebundene Netze"

1. telefon, tv kabel, fttx
2. **PSTN**: Public Switched Telephone Network<br>**POTS**: Plain old telephone service
3. **Waehlverbindung**: Verbindung die das einwaehlen mittels modem erfordert.<br>**Standverbindung**: Permanent aufrecht gehaltene Internetverbindung
4. Integrated Services Digital Network ist ein Standard fuer digitale Kommunication ueber das Telefonnetz
5. Video, Text, Sprache
6. 2 x B-Kanal (Daten)<br>1 x D-Kanal (Steuerinformationen)<br>1 x Synchronisation
7. **Mehrfachkommunikation**: Gleichzeitige Verbindungen zu unterschiedlichen Teilnehmern ueber verschiedene Kommunikationsdienste<br>**Kanalbuendelung**: Mehrere kanaele werden genutzt um uebertragungsrate zu erhoehen
8. 8 / 1
9. Anklopfen, Weiterleitung, Wiederholung, Warteschleife
10. Domain specific language / Domaenenspezifische Sprache: Digital subscriber line
11. Letzte meile
12. Asymetric / Hoeherer down als upstream<br>Mit splitter wird Telefonleitung abgetrennt