|Netzklasse | Präfix | Adressbereich | Netzmaske | Netzlänge (mit Präfix) | Netzlänge (ohne Präfix)  | Hostlänge | Netze | Hosts pro Netz | CIDR Suffix Entsprechung |
| -- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| Klasse A | 0... | 0.0.0.0 – 127.255.255.255 | 255.0.0.0 | 8 Bit | 7 Bit | 24 Bit | 128 | 16.777.214 | /8 |
Klasse B | 10... | 128.0.0.0 – 191.255.255.255 | 255.255.0.0 | 16 Bit | 14 Bit | 16 Bit | 16.384 | 65.534 | /16 |
Klasse C | 110... | 192.0.0.0 – 223.255.255.255 | 255.255.255.0 | 24 Bit | 21 Bit | 8 Bit | 2.097.152 | 254 | /24 |
Klasse D | 1110... | 224.0.0.0 – 239.255.255.255 | Verwendung für Multicast-Anwendungen |
Klasse E | 1111... | 240.0.0.0 – 255.255.255.255 | reserviert (für zukünftige Zwecke) |

Das ursprünglich eingesetzte Konzept der IP-Adressen sah nur eine starre Aufteilung vor. Hierbei waren 8 Bit für die Adressierung des Netzes vorgesehen, die übrigen 24 Bit adressierten einen spezifischen Teilnehmer des Netzes. Bei diesem Konzept waren aber nur 256 Netze möglich. Dies wurde als zu wenig erkannt. Daher wurden im September 1981 durch RFC 791 die sogenannten Netzklassen eingeführt, die diese Aufteilung neu gestalteten.