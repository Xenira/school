# Datenschutz
## Forts. Rechte des Einzelnen bezgl. Datenschutz
- Einsicht
  - Einsichtsrecht u. Herausgabe von Krankenakten
- Informationspflicht bei Datenverstößen (§42 a BDSG)
  - Sobald verantworliche Stelle (Unternehmen, Behörde...) Kenntnis von Missbrauch personenbezogener Daten erlangt (z.B. Ausspähung), müssen die betroffenen informiert werden

## Allgemeine u. berufsspezifische Datenschutzvorschriften (Gesetze)
1. Allgemeine Rechtsgrundlagen des Tatenschutzes geregelt durch Bundesdatenschutzgesetz (BDSG) u. Datenschutzgesetze der Bundesländer (Landesdatenschutzgesetze, LDSG)
1. Weitere ergänzende allgemeine Datenschutzvorschriften
   - EU-Datenschutzrichtlinie (Richtlinie 95/46/EG)
   - Sozialgesetzbuch (SGB I, SGB X)
   - Strafgesetzbuch
1. bereichsspezifische datenschutzrechtliche Vorschriften, z.B.:
   - Landeskrankenhausgesetze
   - Krebsregistergesetze

### Geltungsbereiche:
**BDSG**: Bundesbehörden, z.B. Bundesamt für Strahlenschutz

**LDSG**: Behörden u. Einrichtungen der Bundesländer (z.B. Einrichtungen einer Stadt)

## Datenschutzbeauftragte

1. Aufgaben
   - Für einhaltung des BDSG und weiterer Gesetze sorgen
   - Schulung
1. Ab wieviel Mitarbeiter notwendig?
   - Automatisiert
      - In öffentlichen stellen
      - Unternehmen ab 10 personen
   - Nicht automatisiert
      - Ab 20 personen
1. Kenntnisse / Qualifikation
   - Ausreichend Fachkunde (Nicht eindeutig geregelt)
      - BDSG
      - IT
   - Betrieb muss Fort und Weiterbildung ermöglichen
   - Zuverlässigkeit / Kein eigenes Interesse am Unternehmen / Keine Führungsposition
1. Kündigungsschutz
   - Nur wenn grǘnde für eine fristlose Kündigung vorliegen
   - Auch ein Jahr nach abberufung
   - Berufung auf 5 Jahre
1. Rolle im Unternehmen (Mitarbeiter o. extern)
   - Beides möglich
