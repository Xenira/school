
# Aufgabe 1

**Reklamation - Auftragsnummer 2017/4242**

Sehr geehrte Damen und Herren,

die bestellten Gartenmöbel sind heute bei uns eingetroffen. Beim Auspacken haben wir festgestellt, dass

**5 Gartenstühle** mit der Verpackung so verklebt sind, dass diese nicht unbeschädigt entpackt werden können

und

**3 Gargenstühle** unsauber lackiert worden sind.

Ein Weiterverkauf ist uns bei diesem Zustand der Ware nicht möglich.

Wir hätten die Möglichkeit diese Mängel in unserer eigenen Werkstatt nachzubessern, wenn Sie uns einen Preisnachlass gewähren.

Alternativ bitten wir Sie die Mängel nachzubessern.

Bitte teilen Sie uns mit, wie Sie weiter verfahren wollen.

Freundliche Grüße,

Gebrüder Mühlmeier

# Aufgabe 2

**Reklamation - Auftragsnummer 2017/4242**

Sehr geehrte Damen und Herren,

die bestellten *Schutzgläser* sind heute bei uns eingetroffen. Beim Auspacken haben wir festgestellt, dass die gelieferten

**40 Paar Ersatzgläser, Bestell-Nr. 922-3 für die Schutzbrillen Bestell-Nr. 922-1**

weder in Form noch Größe in die Schutzbrillen passen.

Bitte senden Sie uns die passenden Gläser, sowie die Rücksendeinformationen für die fehlerhaften Gläser zu.

Freundliche Grüße,

Some random dude

# Aufgabe 3

**Lieferungsverzug - Auftragsnummer 2017/4242**

Sehr geehrte Damen und Herren,

die am 28. April 2017 bestellten *2 Sätze Sommerreifen* sind bis jetzt noch nicht bei uns eingetroffen.

Wenn Sie also Ihren Hund lebend wiedersehen wollen begeben Sie sich Morgen um Punkt 12 in den Stadtpark. Bringen Sie die Reifen in Unauffälligen beuteln und kommen Sie alleine. Keine Polizei.

Freundliche Grüße,

Spedition Kurt Bohlmann

# Aufgabe 4
## Aufgabenteil a

**Lieferungsverzug - Auftragsnummer 2017/4242**

Sehr geehrte Damen und Herren,

die am 20. April 2017 bestellten *Bettgarnituren*, welche bis zum 2. Mai 2017 geliefert worden sein sollten, sind bis jetzt noch nicht bei uns eingetroffen.

Wir benötigen diese Bettgarnituren dringend für die Lagerauffüllung.

Bitte liefern Sie die bestellten Waren innerhalb der Nächsten woche.

Freundliche Grüße,

Textilkaufhaus Brinkmann & Söhne

## Aufgabenteil b

**RE: AW: Lieferungsverzug - Auftragsnummer 2017/4242**

Sehr geehrte Damen und Herren,

Leider ist die Lieferung trotz Ihrer versicherungen im Schreiben vom 9. Mai immer noch nicht bei uns eingetroffen.

Bitte sorgen Sie dafür, dass die Lieferung spätestens am **22. Mai 2017** bei uns eingeht. Andernfalls müssen wir vom Kaufvertrag zurücktreten und Ihnen die entstandenen Kosten anrechnen.

Wir sind auf pünktliche Lieferungen angewiesen um unsere Lager gefüllt zu halten und werden dies bei zukünftigen Bestellungen mit einbeziehen.

Freundliche Grüße,

Aachener Weberei AG