
matches = [
    { score: 21, idPerson: 12341 },
    { score: 85, idPerson: 12341 },
    { score: 80, idPerson: 12341 },
    { score: 98, idPerson: 12341 },
    { score: 81, idPerson: 12341 },
]

function auswertung(matches) {
    let result = []
    for (let i = 0; i < matches.length; i++) {
        j = 0

        while (result[j] && result[j].score < matches[i].score) {
            j++
        }

        if (result[j]) {
            temp1 = matches[i]
            while (result[j]) {
                temp2 = result[j]
                result[j] = temp1
                temp1 = temp2
                j++
            }
            result[j] = temp2
        } else {
            result[j] = matches[i]
        }

    }

    console.log('min', result[0])
    console.log('med', result[(result.length - 1) / 2])
    console.log('max', result[result.length - 1])
    console.log(result)
}

auswertung(matches)