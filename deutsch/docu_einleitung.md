Montag, 16. Oktober 2017 12:58 

# Some random docu

1. [Das Unternehmen](#das-unternehmen)
1. [Projektbeschreibung](#projektbeschreibung)
1. [Ist Analyse](#ist-analyse)
1. [Sollkonzept](#sollkonzept)

## Das Unternehmen

Die Digitalisierung macht neue Wege für den Umgang mit bestehendem Geschäft möglich und schafft völlig neue Geschäftsmodelle. Die fme AG unterstützt seit 15 Jahren Kunden weltweit dabei, ihre Digitale Transformation zu meistern, um wettbewerbsfähig zu bleiben und zukünftige Märkte zu erschließen. Dies erreichen wir, indem wir die Geschäftsmodelle unserer Kunden mithilfe existierender und neuer Technologien optimieren oder in Frage stellen und sie beim notwendigen Kulturwandel im Unternehmen beraten. Cloud, Big Data, Business Intelligence, Social Business Collaboration und Enterprise-Content-Management-Technologien, kombiniert mit der Entwicklung von Individualsoftware und der Fähigkeit, die Systeme unserer Kunden 24×7 zu betreiben, helfen uns dabei, einen Mehrwert im Transformationsprozess unserer Kunden beizutragen.

Der Schwerpunkt liegt auf Lösungen für Life Sciences und industrielle Fertigung. Für gesteigerte Qualität und Compliance, gestärkte Wettbewerbsfähigkeit und ein gesichertes Geschäft in der Zukunft, nutzen wir Best-Practice-Lösungen und sorgen auch für die optimale Implementierung. Wir beraten hersteller-, produkt- und plattformunabhängig. Unsere Teams in Deutschland, den USA und Rumänien sind erfahren darin, globale Projekte zu steuern und verhelfen durch Nutzung von Nearshore-Standorten zu niedrigen Kosten.

## Projektbeschreibung

In diesem Dokument werde ich die Entwicklung des firmeninternen Vertragsverwaltungssystems beschreiben. Dazu werde ich zuerst auf den alten Stand analysieren und dann das Soll konzeptionieren.

Ut sed tortor consequat, finibus ex quis, dictum orci. Nulla elementum ligula ligula, vel rhoncus eros consectetur eu. Curabitur lacinia nunc id magna semper, at vehicula urna venenatis. In fringilla elit facilisis eros dignissim iaculis. Donec volutpat volutpat arcu, eget faucibus augue scelerisque eget. Maecenas mattis ornare arcu eu dictum. Sed placerat feugiat odio, vitae fermentum ante. Praesent porta, tortor sed mattis pretium, tellus lorem ornare orci, at mattis neque dui vel risus. Maecenas ex ex, commodo at eros vel, aliquet elementum ante.

## Ist Analyse

Die Rechnungen der Firma muessen fuer mehrere Jahre aufbewart werden und eine Liste aller Vertraege wird nur in Excel gepflegt.

Die eigentlichen Dokumente sind in dem Firmeninternen ECMS hinterlegt. In der Excel-Liste sind dann die eindeutigen Dokumentennummern eingetragen. Im laufe der Zeit ist es nun dazu gekommen, dass die Daten in der Liste inkonsistent geworden sind. Dokumentennummern und eigenschaften treffen zum teil nicht mehr zu.
Des weiteren entsteht eine menge manueller Arbeit, da Mitarbeiter haendisch an das Wiedervorlagedatum erinnert werden muessen.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non interdum tortor. In dapibus semper euismod. Integer tempus non velit sed volutpat. Nullam ex tellus, dictum in lobortis ut, volutpat eget est. Vivamus sapien turpis, bibendum vel erat auctor, finibus tincidunt risus. Proin vel sem et nunc elementum dapibus at lobortis erat. Nam at commodo velit. Fusce aliquam augue venenatis, rhoncus lectus nec, porttitor quam. Nulla eu diam turpis. Proin varius quam non dolor facilisis, a tristique orci condimentum. Nulla porttitor quam odio, at varius purus dignissim et. Integer vel lacus accumsan, fermentum ligula sed, bibendum nunc.

## Sollkonzept

Die Vertraege sollen in dem Dokumentenmanagementsystem abgelegt und in einer Webapp abrufbar sein. Zudem sollen Erinnerungen automatisch versendet werden.

Dafuer wird das Firmeneigene Custom Solution Framework (CSF) verwendet. Es wird ein eigenes Datenmodell und sowohl Backend als auch Frontend erstellt und eine Migration aus der Excel-Liste vorgenommen.

Mauris dictum eget justo in iaculis. Nam nec convallis elit, vitae lacinia sapien. Etiam *elementum* felis sit amet mauris ornare, id consequat turpis vehicula. Sed lobortis malesuada finibus. Nullam vulputate erat eu nisl porta faucibus. Curabitur eget erat in odio ullamcorper feugiat. Sed sed pellentesque nisi. Morbi aliquam, lacus vel imperdiet pulvinar, purus ex mollis felis, pulvinar dignissim libero felis eu ante. Nulla quis lacus et orci volutpat porta nec facilisis nisl. Curabitur ut massa ut ante sodales aliquam sit amet at mi. Nulla rhoncus erat mollis nisl scelerisque, vel aliquet nulla dictum.

Ut ac metus posuere, aliquam nisi sed, lacinia enim. Proin gravida et ex sit amet ultricies. Sed blandit dui sed elit posuere, et varius neque blandit. Vestibulum convallis orci quis *condimentum* aliquam. Vivamus in lacus in tellus semper convallis eu eu mauris. Phasellus quis congue nisl, sed elementum nibh. Nullam fermentum eros a sem varius euismod. Cras consectetur consectetur elit ut elementum. In hac habitasse platea dictumst. Maecenas et molestie dui. Nunc rhoncus vulputate purus, quis varius orci bibendum vel. Quisque pulvinar a dolor at molestie.