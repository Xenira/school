# Grundlagen der Software-Ergonomie

## Aufgaben

1. Beides: Die software muss sowohl gut benutzbar, als auch gut wartbar sein.
1. Es muss an i18n gedacht werden um Internationale benutzer anzusprechen. Die Schrift muss ausreichend gut lesbar sein, so dass Benutzer aus allen Altersgruppen diesen gut erkennen koennen. Eventuelle Docs und Hilfen muessen einfach erreichbar sein.
1. Usebility beschreibt die *Effektivitaet*, *Effizienz* und *Zufriedenstellung*, welche mit der Software erreicht werden kann. Dies bedeutet, dass die Software korrekt arbeitet, eine Arbeitserleichterung darstellt und gerne vom Benutzer genutzt wird.
1. Effektivitaet bedeutet, dass die Software korrekte Ergebnisse liefert. Effizienz bedeutet, dass die Software eine Arbeitserleichterung darstellt.
1. Wenn die Software fuer die auszufuehrenden Aufgaben des Benutzers geeignet ist und die Hauptaufgaben und Benutzereigenschaften unterstuetzt.
1. Dialoggestaltung:
   1. **Aufgabenangemessenheit**: Details button in error msg
   1. **Selbstbeschreibungsfaehigkeit**: Passwort min 12 chars
   1. **Erwartungskonformitaet**: Ok, Abbrechen etc an selber position
   1. **Lernfoerderlichkeit**: Loeschen bestaetigen
   1. **Steuerbarkeit**: Mehrere optionen
   1. **Fehlertoleranz**: Felererkennung + Korrektur
   1. **Individualisierbarkeit**: Dark theme
1. Informationsdarstellung:
   1. **Erkennbarkeit**: Rote Fehlermeldungen
   1. **Unterscheidbarkeit**: Trennstriche / Tabellen
   1. **Lesbarkeit**: Kontrast bg / text | Textgroesse
   1. **Verstaendlichkeit**: Verstaendlicher inhalt
   1. **Klarheit**:
   1. **Kompaktheit** / **Praegnanz**: Nur essenzielle info
   1. **Konsistenz**: Texte an erwarteter stelle
1. #ffffff or #f0f0ed / #4d4d4d
1. https://sovt.de/wp-content/uploads/cebit-2003.pdf<br>
https://www.iip.kit.edu/downloads/Softrwareergonomie_WS0809.pdf
10. 