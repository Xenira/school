# Datenbankmanagementsysteme

## Hierarchische Datenbanken

**Definition**: A hierarchical database model is a data model in which the data is organized into a tree-like structure. The data is stored as records which are connected to one another through links. A record is a collection of fields, with each field containing only one value. The entity type of a record defines which fields the record contains.
![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Hierarchical_Model.svg/548px-Hierarchical_Model.svg.png)

**Query:** Commands in DB structure (IMS).

**Usecases:** Currently hierarchical databases are still widely used especially in applications that require very high performance and availability such as banking and telecommunications. One of the most widely used commercial hierarchical databases is IMS. Another example of the use of hierarchical databases is Windows Registry in the Microsoft Windows operating systems.

**Pros:**
- Faster navigation
- Better understanding / clarity

**Cons:**
- Rigid structure

## Objektrelationale Datenbanken
**Definition:** An object-relational database (ORD), is similar to a relational database, but with an object-oriented database model: objects, classes and inheritance are directly supported in database schemas and in the query language. In addition, just as with pure relational systems, it supports extension of the data model with custom data-types and methods.

**Query:**<br>
Query language eg.:
```sql
CREATE TABLE Customers (
  Id           Cust_Id     NOT NULL  PRIMARY KEY,
  Name         PersonName  NOT NULL,
  DOB          DATE        NOT NULL
);
SELECT Formal( C.Id )
  FROM Customers C
  WHERE BirthDay ( C.DOB ) = TODAY;
```

**Usecases:** Bridge gap between ORDB and OODB. Nowerdays an object-relational mapping (ORM) software is used more commonly.

**Pros:**
- Combines advantages from relational and object DB

**Cons:**
- Performance (Größere / Komplexere DBs)

## Objektorientierte Datenbanken
**Definition:** A OODBMS database is essentially a persistent object store for software written in an object-oriented programming language, with a programming API for storing and retrieving objects, and little or no specific support for querying.
![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Object-Oriented_Model.svg/598px-Object-Oriented_Model.svg.png)

**Usecases:** Object databases based on persistent programming acquired a niche in application areas such as engineering and spatial databases, telecommunications, and scientific areas such as high energy physics and molecular biology.

Another group of object databases focuses on embedded use in devices, packaged software, and real-time systems.

Not commonly used.

**Query:** Depends on DBMS

**Pros:**
- Reduced Maintenance
- Model real world
- Complex datatypes

**Cons:**
- Performance (depends on usecase)
- Lack of datamodel
- Lock on object level
- Security

## NoSQL Datenbanken
**Definition:** A NoSQL database provides a mechanism for storage and retrieval of data that is modeled in means other than the tabular relations used in relational databases

**Usecases:** Cloud and scalable applications.

**Datenschema:** e.g. key-value, wide column, graph, or document

**Query:** Abhängig von DBMS.

**Pros:**
- Horizontal scalebility
- Some operations are faster
- Flexible

**Cons:**
- Mostly no ACID transactions
- eventual consistency

---

## Is there a need to normalize the data?

**Yes:** Use relational.<br>
**No:** Use document.