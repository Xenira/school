# Konvertiert von 'MDB2MySQL v1.6.2' zu MySQL
# � 2000 - 2002 Daniel Petri, www.mdb2mysql.de


CREATE TABLE Dozent
     (
     DNR  INT NOT NULL AUTO_INCREMENT,
     Name  CHAR(30),
     UESatz  DECIMAL(20,4),
     PRIMARY KEY (DNR)
     ) ;

INSERT INTO Dozent VALUES (1, 'Wirtgen', 60);
INSERT INTO Dozent VALUES (2, 'Schult', 65);
INSERT INTO Dozent VALUES (3, 'Schneider', 50);
INSERT INTO Dozent VALUES (4, 'G�ppert', 70);
INSERT INTO Dozent VALUES (5, 'Hamblen', 70);
INSERT INTO Dozent VALUES (6, 'Dohlen', 55);
INSERT INTO Dozent VALUES (7, 'Heidert', 60);
INSERT INTO Dozent VALUES (8, 'Kruse', 55);
INSERT INTO Dozent VALUES (9, 'Feldmann', 70);
INSERT INTO Dozent VALUES (10, 'Seibold', 65);
INSERT INTO Dozent VALUES (11, 'Bachmann', 22.5);

CREATE TABLE Inhalt
     (
     INR  INT NOT NULL AUTO_INCREMENT,
     Inhalt  CHAR(50),
     UEPreis  DECIMAL(20,4),
     DauerUE  SMALLINT,
     PRIMARY KEY (INR)
     );

INSERT INTO Inhalt
VALUES (
     1, 
     'MS-Office Grundlagen', 
     20, 
     16 );

INSERT INTO Inhalt
VALUES (
     2, 
     'Java Anf�nger', 
     22, 
     48 );

INSERT INTO Inhalt
VALUES (
     3, 
     'C++ Fortgeschrittene', 
     30, 
     32 );

INSERT INTO Inhalt
VALUES (
     4, 
     'Konfiguaration von Firewalls', 
     25, 
     8 );

INSERT INTO Inhalt
VALUES (
     5, 
     'Einrichtung eines Webservers', 
     18, 
     16 );

INSERT INTO Inhalt
VALUES (
     6, 
     'Teamarbeit', 
     15, 
     40 );

INSERT INTO Inhalt
VALUES (
     7, 
     'Projektmanagement', 
     12, 
     16 );

INSERT INTO Inhalt
VALUES (
     8, 
     'Qualit�tssicherung', 
     20, 
     32 );

INSERT INTO Inhalt
VALUES (
     9, 
     'Java Fortgeschrittene', 
     30, 
     40 );

INSERT INTO Inhalt
VALUES (
     10, 
     'C++ Experten', 
     30, 
     80 );

INSERT INTO Inhalt
VALUES (
     11, 
     'Systemadministration WIN NT', 
     15, 
     32 );

INSERT INTO Inhalt
VALUES (
     12, 
     'LINUX Installation', 
     20, 
     20 );

INSERT INTO Inhalt
VALUES (
     13, 
     'C++ Anf�nger', 
     15, 
     32 );

INSERT INTO Inhalt
VALUES (
     14, 
     'Access Einf�hrung', 
     15, 
     32 );

CREATE TABLE Lehrgangsort
     (
     LONR  INT NOT NULL AUTO_INCREMENT,
     Bezeichnung  CHAR(40),
     PRIMARY KEY (LONR)
     );

INSERT INTO Lehrgangsort VALUES (1, 'Hotel Savoy K�ln');
INSERT INTO Lehrgangsort VALUES (2, 'Friedrich-List-Berufskolleg Bonn');
INSERT INTO Lehrgangsort VALUES (3, 'Hotel Petersberg K�nigswinter');


CREATE TABLE Lehrgang
     (
     LNR  INT NOT NULL AUTO_INCREMENT,
     INR  INT ,
     Beginn  DATETIME,
     DNR  INT ,
     LONR  INT ,
     PRIMARY KEY (LNR),
	 FOREIGN KEY (INR) references Inhalt(INR),
	 FOREIGN KEY (DNR) references Dozent(DNR),
	 FOREIGN KEY (LONR) references Lehrgangsort(LONR)
     );

INSERT INTO Lehrgang
VALUES (
     1, 
     2, 
     '2000-06-16 00:00:00', 
     10, 
     1 );

INSERT INTO Lehrgang
VALUES (
     2, 
     2, 
     '2000-06-15 00:00:00', 
     9, 
     2 );

INSERT INTO Lehrgang
VALUES (
     3, 
     8, 
     '2000-11-23 00:00:00', 
     8, 
     1 );

INSERT INTO Lehrgang
VALUES (
     4, 
     14, 
     '2000-02-08 00:00:00', 
     7, 
     1 );

INSERT INTO Lehrgang
VALUES (
     5, 
     3, 
     '2000-01-15 00:00:00', 
     5, 
     2 );

INSERT INTO Lehrgang
VALUES (
     6, 
     9, 
     '2000-06-30 00:00:00', 
     4, 
     2 );

INSERT INTO Lehrgang
VALUES (
     7, 
     1, 
     '2000-01-25 00:00:00', 
     4, 
     1 );

INSERT INTO Lehrgang
VALUES (
     8, 
     9, 
     '2000-06-30 00:00:00', 
     3, 
     2 );

INSERT INTO Lehrgang
VALUES (
     9, 
     6, 
     '2000-10-10 00:00:00', 
     2, 
     3 );

INSERT INTO Lehrgang
VALUES (
     10, 
     11, 
     '2000-03-30 00:00:00', 
     1, 
     1 );

INSERT INTO Lehrgang
VALUES (
     11, 
     7, 
     '2000-08-30 00:00:00', 
     7, 
     3 );



CREATE TABLE Ort
     (
     PLZ  CHAR(6),
     Ort  CHAR(30),
     PRIMARY KEY (PLZ)
     );

INSERT INTO Ort VALUES ('16845', 'Holzhausen');
INSERT INTO Ort VALUES ('22926', 'Birkenbusch');
INSERT INTO Ort VALUES ('23898', 'K�hsen');
INSERT INTO Ort VALUES ('36115', 'Ehrenberg');
INSERT INTO Ort VALUES ('38275', 'Haverlah');
INSERT INTO Ort VALUES ('45881', 'Gelsenkirchen');
INSERT INTO Ort VALUES ('46117', 'Oberhausen');
INSERT INTO Ort VALUES ('47259', 'Duisburg');
INSERT INTO Ort VALUES ('47929', 'Grefrath');
INSERT INTO Ort VALUES ('50769', 'K�ln');
INSERT INTO Ort VALUES ('50858', 'K�ln');
INSERT INTO Ort VALUES ('53177', 'Bonn');
INSERT INTO Ort VALUES ('53229', 'Bonn');
INSERT INTO Ort VALUES ('53474', 'Bad Neuenahr-Ahrweiler');
INSERT INTO Ort VALUES ('53547', 'Dattenberg');
INSERT INTO Ort VALUES ('56295', 'Lonnig');
INSERT INTO Ort VALUES ('56584', 'R�schied');
INSERT INTO Ort VALUES ('59075', 'Hamm');
INSERT INTO Ort VALUES ('73486', 'Adelmannsfelden');
INSERT INTO Ort VALUES ('83739', 'Hei�kistler');
INSERT INTO Ort VALUES ('84405', 'Loh');
INSERT INTO Ort VALUES ('91469', 'Erlachskirchen');


CREATE TABLE Teilnehmer
     (
     TNR  INT NOT NULL AUTO_INCREMENT,
     Name  CHAR(20),
     PLZ  CHAR(6) ,
     PRIMARY KEY (TNR),
	 FOREIGN KEY (PLZ) references ORT (PLZ)
     );

INSERT INTO Teilnehmer VALUES (1, 'Battner', '73486');
INSERT INTO Teilnehmer VALUES (2, 'Milz', '16845');
INSERT INTO Teilnehmer VALUES (3, 'J�ger', '23898');
INSERT INTO Teilnehmer VALUES (4, 'Grifftith', '56584');
INSERT INTO Teilnehmer VALUES (5, 'Betz', '53177');
INSERT INTO Teilnehmer VALUES (6, 'Gr�ndgens', '56295');
INSERT INTO Teilnehmer VALUES (7, 'Bach', '83739');
INSERT INTO Teilnehmer VALUES (8, 'Eisenmann', '84405');
INSERT INTO Teilnehmer VALUES (9, 'Kelter', '53474');
INSERT INTO Teilnehmer VALUES (10, 'Bachmann', '47259');
INSERT INTO Teilnehmer VALUES (11, 'Ahmendt', '50858');
INSERT INTO Teilnehmer VALUES (12, 'Jacobs', '56295');
INSERT INTO Teilnehmer VALUES (13, 'Dotzler', '46117');
INSERT INTO Teilnehmer VALUES (14, 'Sterling', '50769');
INSERT INTO Teilnehmer VALUES (15, 'Escher', '59075');
INSERT INTO Teilnehmer VALUES (16, 'Borchers', '91469');
INSERT INTO Teilnehmer VALUES (17, 'Zobel', '84405');
INSERT INTO Teilnehmer VALUES (18, 'Zimmermann', '47929');
INSERT INTO Teilnehmer VALUES (19, 'Br�dner', '53229');
INSERT INTO Teilnehmer VALUES (20, 'Babbage', '56584');
INSERT INTO Teilnehmer VALUES (21, 'Coy', '84405');

CREATE TABLE Teilnahme
     (
     TNR  INT, FOREIGN KEY (TNR) references Teilnehmer(TNR),
     LNR  INT, FOREIGN KEY (LNR) references Lehrgang(LNR),
     PRIMARY KEY (TNR, LNR)
     );


INSERT INTO Teilnahme VALUES (1, 10);
INSERT INTO Teilnahme VALUES (2, 1);
INSERT INTO Teilnahme VALUES (3, 3);
INSERT INTO Teilnahme VALUES (3, 6);
INSERT INTO Teilnahme VALUES (3, 9);
INSERT INTO Teilnahme VALUES (4, 7);
INSERT INTO Teilnahme VALUES (5, 2);
INSERT INTO Teilnahme VALUES (5, 8);
INSERT INTO Teilnahme VALUES (5, 9);
INSERT INTO Teilnahme VALUES (6, 6);
INSERT INTO Teilnahme VALUES (7, 2);
INSERT INTO Teilnahme VALUES (7, 8);
INSERT INTO Teilnahme VALUES (7, 9);
INSERT INTO Teilnahme VALUES (7, 11);
INSERT INTO Teilnahme VALUES (8, 1);
INSERT INTO Teilnahme VALUES (8, 4);
INSERT INTO Teilnahme VALUES (8, 7);
INSERT INTO Teilnahme VALUES (9, 5);
INSERT INTO Teilnahme VALUES (9, 9);
INSERT INTO Teilnahme VALUES (10, 5);
INSERT INTO Teilnahme VALUES (11, 5);
INSERT INTO Teilnahme VALUES (11, 9);
INSERT INTO Teilnahme VALUES (12, 4);
INSERT INTO Teilnahme VALUES (12, 5);
INSERT INTO Teilnahme VALUES (12, 7);
INSERT INTO Teilnahme VALUES (13, 1);
INSERT INTO Teilnahme VALUES (13, 3);
INSERT INTO Teilnahme VALUES (13, 8);
INSERT INTO Teilnahme VALUES (14, 1);
INSERT INTO Teilnahme VALUES (14, 8);
INSERT INTO Teilnahme VALUES (15, 3);
INSERT INTO Teilnahme VALUES (15, 7);
INSERT INTO Teilnahme VALUES (16, 4);
INSERT INTO Teilnahme VALUES (16, 7);
INSERT INTO Teilnahme VALUES (17, 2);
INSERT INTO Teilnahme VALUES (17, 4);
INSERT INTO Teilnahme VALUES (17, 7);

INSERT INTO Teilnahme VALUES (19, 10);
INSERT INTO Teilnahme VALUES (20, 3);
INSERT INTO Teilnahme VALUES (20, 10);
INSERT INTO Teilnahme VALUES (21, 8);
INSERT INTO Teilnahme VALUES (21, 10);
INSERT INTO Teilnahme VALUES (3, 1);
