Dienstag, 02. Mai 2017 01:02 

**SELECT** p.P_Bezeichnung, **COUNT(w.W_ID**) FROM** Partei p **JOIN** Waehler **ON** w.W_P_ID = p.Id **GROUP BY** p.P_Bezeichnung **ORDER BY** p.Bezeichnung

**SELECT** p.Bezeichnung, w.c **FROM** Partei p **LEFT JOIN** (**SELECT** **COUNT**(w.W_ID) c, w.W_P_ID **FROM** Waehler w **JOIN** Schulabschluss s **ON** w.W_S_ID = s.S_ID **AND** s.S_Bezeichnung = 'Fachoberschulreife' **GROUP BY** W_P_ID) w **ON** w.W_P_ID = p.Id **ORDER BY** p.Bezeichnung

**SELECT** p.P_Bezeichnung, b.B_Bezeichung **COUNT(**W_ID**) FROM** Partei p **JOIN** Waehler w **ON** p.P_Id = w.W_P_ID **JOIN** Bundesland b **ON** w.W_B_ID = b.B_Id **AND** b.B_Bezeichnung like 'N%' **GROUP BY** p.P_Bezeichnung, b.B_Bezeichnung **ORDER BY** p.P_Bezeichung **DESC**, b.B_Bezeichnung

**SELECT** * **FROM**