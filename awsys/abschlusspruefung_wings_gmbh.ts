var strecken: any[]

function findeRoute(gewicht: Number): Number
{
	let index = -1;
	let preis = 0;
	for(let i = 0; i < strecken.length; i++) {
		var pos = 0;
		var flug = strecken[i][0];
		var kosten = 0;
		var ausreichendKapazitaet = true;
		
		while (strecken[i][pos + 1] != null)  {
			if (holeStreckeGewicht(flug, strecken[i][pos + 1]) < gewicht) {
				ausreichendKapazitaet = false;
				break;
			}
			kosten += holeStreckePreis(flug, strecken[i][pos + 1]);
			pos ++;
			flug = strecken[i][0]
		}
		
		if (!ausreichendKapazitaet) {
			continue;
		}
		
		if (preis > kosten || index == -1) {
			index = i;
			preis = kosten;
		}
	}
	
	return index;    
}