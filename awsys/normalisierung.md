# Normalisierung
## 1. Normalform

Attribute werden **atomar**

| Auftrags Nr | KD Nr. | KD Name | KD Straße | KD PLZ | KD Ort | KD Telefon | Auftragsdatum | Pr Nr | Menge | Pr Name |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |

## 2. Normalform
Abhängigkeit von **einem** schlüssel / **einem** eindeutigen

*Auftrag*

| Auftrags Nr | KD Nr. | KD Name | KD Straße | KD PLZ | KD Ort | KD Telefon | Auftragsdatum |
| --- | --- | --- | --- | --- | --- | --- | --- |

*Auftragsposition*

| Auftrags Nr. | Pr Nr | Menge |
| --- | --- | --- |

*Produkt*

| Pr Nr | Pr Name |
| --- | --- |

## 3. Normalform
Keine abhängigkeit von nicht schlüssel attributen

*Auftrag*

| Auftrags Nr | KD Nr | Auftragsdatum |
| --- | --- | --- |

*Auftragsposition*

| Auftrags Nr. | Pr Nr | Menge |
| --- | --- | --- |

*Produkt*

| Pr Nr | Pr Name |
| --- | --- |

*Kunde*

| KD Nr. | KD Name | KD Straße | KD PLZ | KD Ort | KD Telefon |
 --- | --- | --- | --- | --- | --- |

 # Aufgabe 4. Normalisieren
**Original**
| VorgangsID | DelinquentID | Anrede | Delinquent | Geburtsdatum | Adresse | Delikt | Datum | Dokument | Bearbeiter |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| 303 | 7887 | Herr | Jon, Doe | 01.07.1988 | 03669 Astadt Franzgasse 3 | Fahrerflucht, Drogenmissbrauch | 30.03.2017 | Reisepass, Fahrerlaubnis | Hansen, Klaus |

**Normalisiert:**

 *Vorgang*
 | VorgangsID (PK) | DelinquentID (FK) | Datum | BearbeiterID |
 | --- | --- | --- | --- |

 *Delinquent*
 | DelinquentID (PK) | Anrede | Vorname | Nachname | Geburtsdatum | Adresse |
 | --- | --- | --- | --- | --- | --- |

 *Delikt*
 | DeliktID (PK) | Beschreibung |
 | --- | --- |

 *VorgangDelikt*
 | VorgangsID (PK FK) | DeliktID (PK FK) |
 | --- | --- |

 *Dokument*
 | DokumentID (PK) | Beschreibung |
 | --- | --- |

 *VorgangDokument*
 | VorgangsID (PK FK) | DokumentID (PK FK) |
 | --- | --- |

 *Bearbeiter*
 | BearbeiterID (PK) | Vorname | Nachname |
 | --- | --- | --- |