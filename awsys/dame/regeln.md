# Russian Draughts

## Spielbrett
Gespielt wird auf einem 8x8 "Schachbrett" (Weiß ist in der unteren rechten Ecke). Jeder Spieler hat 12 Spielsteine, die in den drei reihen, welche dem Spieler am nähchsten sind, auf den schwarzen Feldern aufgebaut werden. Ein Spieler hat dabei weiße Steine und der andere schwarze. Der Spieler mit den hellen Steinen beginnt das Spiel.

![](http://1.1.1.5/bmi/ludoteka.com/darutab.gif)

## Regeln
Es wird abwechselnd gezogen. Normale steine können sich dabei nur ein Feld diagonal nach vorne bewegen. Wenn ein gegnerischer Stein dabei geschlagen werden kann (Durch "überspringen" des steins) so muss dies getan werden. Wenn nach dem schlagen eines Gegnerischen steines ein weiterer geschlagen werden kann, so muss auch dies getan werden. Dabei muss allerdings nicht der Weg gewählt werden, auf dem man die meisten Gegner schlägt.

### Könige
Erreicht man mit einem Spielstein die letzte gegnerische Reihe, so wird der Stein zum König (Ein zweiter stein wird auf den König gelegt). Ein könig kann unbegrenzt viele Felder diagonal gehen. Wenn ein Stein nach dem schlagen eines Gegners auf der letzten Reihe landet, wird dieser sofort zum König und kann als König erneut schlagen.

## Ende des Spiels
Sobald ein Spieler keine gültigen Züge mehr machen kann verliert dieser.

### Unentschieden
Ein unentschieden tritt auf, wenn keiner der Spieler das Spiel mehr vorran bringen kann. Dies ist der Fall, wenn die selbe Position drei mal hintereinander auftritt.
Zudem tritt ein Unentschieden auf, wenn sich beide Spieler auf eins einigen oder ein Spieler mit drei Königen gegen einen Spieler mit einem König den gegnerischen König nicht in 15 zügen besiegen kann.