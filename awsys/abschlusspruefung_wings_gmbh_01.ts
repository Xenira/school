interface Flug {
    getDatum(): Date;
    getFreiePlaetze(): number;
    getPreis(): number;
}

const Linien_Fluege: Flug[] = []

// Bitte schlagen Sie mich nicht
function erstelleFluege(Datum: Date, Plaetze: number) : Flug[] {
    let Auswahl_Fluege = Linien_Fluege.filter(f => f.getDatum() == Datum &&
                                f.getFreiePlaetze() >= Plaetze);
    return Auswahl_Fluege.sort(a => a.getPreis());
}