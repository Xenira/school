# Datenintegrität

## Deklarative Constraints
**Bedingungen in der ```Create``` anweisung**

Bsp.:
- Primary Key
- Forign Key
- unique
- not null
- check
- auto_increment

### Foreign Key
Der Fremdschlüssel stellt die **referentielle Integrität** sicher. D.h., FK müssen auf existierende Tupel einer anderen Relation verweisen (evtl. auch ```NULL```-Wert möglich)

## Prozedurale Constraints
### Trigger
- Ziel: Abbilden komplexerer Bedingungen
- Verfahren: Aufruf duch das Auftreten eines Ereignisses (```INSERT```, ```DELETE``` oder ```UPDATE```) in einer Tabelle

**Grundstruktur:**
```sql
CREATE TRIGGER <t.name>
{BEFORE | AFTER} {INSERT | DELETE | UPDATE}
ON <tbl.name>
FOR EACH ROW
<t.body>
```

#### Übungsaufgaben:
1. Ergänzen Sie im Lehrgang die Teilnehmerzahl
1. Beim Anmelden eines Teilnehmers soll die Anzahl angepasst werden
1. Es ist die Teilnehmeranzal pro Kurs festzulegen. Wenn sich mehr Kunden als Plätze anmelden, ist eine Warteliste zu führen. Eine Woche vor Kursbeginn ist keine Anmeldung mehr anzunehmen.

**1 + 2)**
```sql
ALTER TABLE `Lehrgang` ADD `Teilnehmer` INT NOT NULL DEFAULT '0' AFTER `LONR`, ADD `MaxTeilnehmer` SMALLINT NOT NULL AFTER `Teilnehmer`;

CREATE TRIGGER `TG_AddParticipant` AFTER INSERT ON `Teilnahme` FOR EACH ROW 
UPDATE Lehrgang SET Lehrgang.Teilnehmer = Lehrgang.Teilnehmer + 1 WHERE Lehrgang.LNR = NEW.LNR

CREATE TRIGGER `TG_RemoveParticipant` AFTER DELETE ON `Teilnahme` FOR EACH ROW 
UPDATE Lehrgang SET Lehrgang.Teilnehmer = Lehrgang.Teilnehmer - 1 WHERE Lehrgang.LNR = OLD.LNR
```
**3)**
```sql
ALTER TABLE `Teilnahme` ADD `Registered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() AFTER `LNR`;

CREATE TRIGGER `TG_RegistrationGuard` BEFORE INSERT ON `Teilnahme`
FOR EACH ROW
BEGIN
    DECLARE closed BOOLEAN;
    SELECT Lehrgang.Beginn < NOW() + INTERVAL 1 WEEK INTO closed FROM Lehrgang WHERE Lehrgang.LNR = NEW.LNR;
    
    IF (closed) THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Registration has to be done at least 1 week before the start of the course.';
    END IF;
END;
```