# Prüfziffer

**```C-Style``` Pseudocode:**
```ts
boolean function pruefeID(id: string) {
    result = 0;
    for (i = 0; i < id.length - 1; i++) {
        If (i % 2 != 0)
            result = result + id[i]
        Else
            result = result + quersumme(id[i] * 2)
    }
    
    diff = 10 - result % 10
    return id[id.length] == diff
}

function quersumme(zahl: int) {
    result = 0
    do {
        result = result + zahl % 10
        zahl = zahl / 10
    } while (zahl != 0)
}
```

# Route
```ts
function findeRoute(gewicht: integer): integer {
    bestIndex = -1;
    bestPrice = 0;
    priceChanged = false
    for (i = 0; i < routen.length; i++) {
        j = 1;
        price = 0;
        lastStop = routen[i][0];
        while (routen[i][j] AND
            holeStreckeGewicht(lastStop, routen[i][j]) > gewicht AND
            (price < bestPrice OR !priceChanged)) {
            price += holeStreckePreis(lastStop, routen[i][j])
            lastStop = routen[i][j]
            j++;
        }

        if (!routen[i][j]) {
            next;
        }
        if (price < bestPrice OR !priceChanged)
            bestIndex = i
            bestPrice = pricebestPrice < 0
            priceChanged = true
        }
    }

    return bestIndex;
}
```

# Erstelle Fluege
```ts
function erstelleFluege(Datum: Date, Plaetze: Integer): Flug[] {
    fluege = new Flug[Linien_Fluege.length]
    i = 0;
    for (Flug f in Linien_Fluege) {
        if (f.getFlugDatum() != Datum AND
            f.getFreiePlaetze() < Plaetze) {
            next;
        }

        fluege[i] = f
        i++;
    }

    return sortAndTrimArray(fluege, i);
}

function sortAndTrimArray<T>(array: T[], length: Integer) {
    result = new T[length]
    for (j = 0; j < length; j++) {
        preis = -1
        index = -1
        preisChanged = false
        for (k = 0; k < length) {
            if (array[k] AND (array[k].getPreis() < preis OR !preisChanged)) {
                preis = array[k].getPreis();
                index = k;
                preisChanged = true;
            }
        }

        result[j] = array[index];
        array[index] = null;
    }

    return result;
}
```