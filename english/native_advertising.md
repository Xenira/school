# Native Advertising

Native advertising describes the practice of hiding advertisements within articles that look like normal news articles. The consumer is usually not able to tell the difference between real news articles and advertisement.
This is troublesome because companies acquire the ability to manipulate a lot of people through "reputable" news outlets.

The Internet has lots of these hidden ads.
Starting with the suddle ads on the google results page, going over Youtubers doing untagged product placement up to real scams like fake virus warnings, selling freeware and so on.