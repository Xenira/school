# Enquiry

**FROM:**	mr.x@construct-world.pl
**TO:**		mr.y@hauch-werkzeuge.de

Dienstag, 09. Mai 2017 10:49 
**Tool supplier for DIY store**

Dear Sir/Madam,

We saw your booth at a trade show in Hannover displaying your selection of tools for every occasion.

We are a large DIY store in Poland with over 9000 employees.

We are looking for a new supplier of quality tools for our new stores. Therefore we would ask you to send us a copy of your latest catalogue and price list.

Please also inform us about your export prices and possible discounts as well as terms of payment and delivery times.

As we would like to test your tools, we would appreciate it if you could send us some samples.

Thank you for your attention to this enquiry and we look forward to hearing from you.

Yours sincerly,

*Mr. X*
Construct World

# Offer

**FROM:**	mr.y@hauch-werkzeuge.de
**TO:**		mr.x@construct-world.pl

Dienstag, 09. Mai 2017 10:50 
**Your enquiry**

Dear Mr. X,

Thank you for your enquiry and your intrest in our products.

We are sending you our latest brochure and price list as an attachment.
For orders of 2.500€ or more we are able to grant a discount of five per cent.
For large orders (over 500 units) we require a delivery period of 5 - 10 working days.
Smaller orders can be delivered from stock and within 2 - 4 working days.

Our usual terms of payment are by bank transfer to our account with the ABC bank.

One sample case of ratchets, sockets, wrenches and screwdrivers has been dispatched to you this morning and should reach you in the next few days.

Thank you again for your enquiry. We look forward to establish regular business with Construct World.

Yours sincerely,

*Mr. Y*
Hauch Werkzeuge KG